class Person:
    raw = {'account_username': 'None', 'account_rights': 'None', 'account_id': 'None', 'info_id': 'None', 'info_voornaam': 'None', 'info_tussenvoegsel': '', 'info_achternaam': '', 'info_adres': 'None', 'info_email': 'None', 'info_huisnummer': 'None', 'info_extra': 'None', 'info_geboorte': 'None', 'info_postcode': 'None', 'info_telefoon': 'None', 'info_indienst': 'None', 'info_plaats': 'None', 'info_telefoon2': 'None', 'info_geboorteplaats': 'None', 'functie_naam': 'None', 'functie_kleur': 'None', 'functie_id': 'None', 'functie_afdeling_id': 'None', 'info2_id': 'None', 'info2_info_id': 'None', 'info2_max_dagen': 'None', 'info2_max_eind': 'None', 'info2_opeenvolgend': 'None', 'info2_registreren': 'None', 'info2_werkdag_maandag': 'None', 'info2_werkdag_dinsdag': 'None', 'info2_werkdag_woensdag': 'None', 'info2_werkdag_donderdag': 'None', 'info2_werkdag_vrijdag': 'None', 'info2_werkdag_zaterdag': 'None', 'info2_werkdag_zondag': 'None', 'info2_ut_id': 'None', 'info2_werkweek': 'None', 'info2_klokcode': 'None', 'info2_profielfoto': 'None', 'info2_personeelsnummer': 'None', 'info2_status': 'None', 'info2_taal': 'None'}
    name = ""
    id = ""
    
    def __init__(self,people,uid = "",name=""):
        if uid == "*" or name == "*":
            self.raw = {'account_username': '*', 'account_rights': '*', 'account_id': '*', 'info_id': '*', 'info_voornaam': '*', 'info_tussenvoegsel': '*', 'info_achternaam': '*', 'info_adres': '*', 'info_email': '*', 'info_huisnummer': '*', 'info_extra': '*', 'info_geboorte': '*', 'info_postcode': '*', 'info_telefoon': '*', 'info_indienst': '*', 'info_plaats': '*', 'info_telefoon2': '*', 'info_geboorteplaats': '*', 'functie_naam': '*', 'functie_kleur': '*', 'functie_id': '*', 'functie_afdeling_id': '*', 'info2_id': '*', 'info2_info_id': '*', 'info2_max_dagen': '*', 'info2_max_eind': '*', 'info2_opeenvolgend': '*', 'info2_registreren': '*', 'info2_werkdag_maandag': '*', 'info2_werkdag_dinsdag': '*', 'info2_werkdag_woensdag': '*', 'info2_werkdag_donderdag': '*', 'info2_werkdag_vrijdag': '*', 'info2_werkdag_zaterdag': '*', 'info2_werkdag_zondag': '*', 'info2_ut_id': '*', 'info2_werkweek': '*', 'info2_klokcode': '*', 'info2_profielfoto': '*', 'info2_personeelsnummer': '*', 'info2_status': '*', 'info2_taal': '*'}
        else:
            for i in people:
                if str(uid) == i['account_id']or(name != "" and name.lower() in f"{i['account_username']}\n{i['info_voornaam']} {i['info_achternaam']}\n{i['info_voornaam']} {i['info_tussenvoegsel']} {i['info_achternaam']}".lower()):
                    self.raw = i
                    break
        # else:
            # raise KeyError(f"No person with id '{uid}' or name '{name}' ")
        self.name = f"{self.raw['info_voornaam']} {self.raw['info_achternaam']}" if self.raw['info_tussenvoegsel'] == "" else f"{self.raw['info_voornaam']} {self.raw['info_tussenvoegsel']} {self.raw['info_achternaam']}"
        self.id = self.raw['account_id']
    def __repr__(self) -> str:
        return self.name
    def __eq__(self, __value: object) -> bool:
        try:
            return (self.id == __value.id or self.id == "*" or __value.id == "*" ) and type(self) == type(__value)
        except:
            return False
class Function:
    raw = {'functie_naam': 'None', 'functie_kleur': 'None', 'functie_id': 'None', 'functie_afdeling_id': 'None'}
    id = ""
    name = ""
    def __init__(self,func,fid="",name="") -> None:
        if fid == "*" or name == "*":
            self.raw = {'functie_naam': '*', 'functie_kleur': '*', 'functie_id': '*', 'functie_afdeling_id': '*'}
        else:
            for i in func:
                if str(fid) == i['functie_id']or(name != "" and name.lower() in f"{i['functie_naam']}".lower()):
                    self.raw = i
                    break
        self.id = self.raw["functie_id"]
        self.name = self.raw["functie_naam"]
    def __repr__(self) -> str:
        return self.name
    def __eq__(self, __value: object) -> bool:
        try:
            return (self.id == __value.id or self.id == "*" or __value.id == "*" ) and type(self) == type(__value)
        except:
            return False
class Team:
    raw = {'team_id': 'None', 'team_naam': 'None', 'team_telefoon': 'None', 'team_adres': 'None', 'team_postcode': 'None', 'team_plaats': 'None', 'team_kvk': 'None'}
    id = ""
    name = ""
    def __init__(self,teams,tid="",name="") -> None:
        if tid == "*" or name == "*":
            self.raw = {'team_id': '*', 'team_naam': '*', 'team_telefoon': '*', 'team_adres': '*', 'team_postcode': '*', 'team_plaats': '*', 'team_kvk': '*'}
        for i in teams:
            if str(tid) == i['team_id']or(name != "" and name.lower() in f"{i['team_naam']}".lower()):
                self.raw = i
                break
        self.id = self.raw["team_id"]
        self.name = self.raw["team_naam"]
    def __repr__(self) -> str:
        return self.name
    def __eq__(self, __value: object) -> bool:
        try:
            return (self.id == __value.id or self.id == "*" or __value.id == "*" ) and type(self) == type(__value)
        except:
            return False
class scheduleitem:
    raw = {'teamr_id': 'None', 'teamr_team_id': 'None', 'teamr_rooster_id': 'None', 'rooster_id': 'None', 'rooster_datum': 'None', 'rooster_begin': 'None', 'rooster_eind': 'None', 'rooster_info_id': 'None', 'rooster_functie_id': 'None', 'rooster_publiceren': 'None', 'rooster_pauze': '00:00:None', 'rooster_tijd_incorrect': 'None'}
    person = Person
    date = ""
    starttime = ""
    endtime = ""
    id = ""
    def __init__(self,people,func,teams,raw) -> None:
        self.raw = raw
        self.id = self.raw['rooster_id']
        self.date = self.raw['rooster_datum']
        self.starttime = self.raw['rooster_begin']
        self.endtime = self.raw['rooster_eind']
        self.person = Person(people,self.raw['rooster_info_id'])
        self.function = Function(func,self.raw["rooster_functie_id"])
        self.team = Team(teams,self.raw["teamr_team_id"])

    def __repr__(self) -> str:
        return f"{str(self.person).ljust(25)} {self.date} {self.starttime}-{self.endtime}\t{str(self.function).ljust(20)}{self.team}"
    def __eq__(self, __value: object) -> bool:
        try:
            return (self.id == __value.id or self.id == "*" or __value.id == "*" ) and type(self) == type(__value)
        except:
            return False
class filter:
    filteritem = None
    whatever = False
    def __init__(self,payload,filtertype="person",id="",name="") -> None:
        if name == "" and id == "":
            name,id = "**"
        if type(payload) == dict:
            payload = payload[filtertype]
        self.filtertype = filtertype
        if self.filtertype == "person":
            self.filteritem = Person(payload,id,name)
        elif self.filtertype == "team":
            self.filteritem = Team(payload,id,name)
        elif self.filtertype == "function":
            self.filteritem = Function(payload,id,name)
        else:
            self.whatever = True
    def __repr__(self) -> str:
        return f"{self.filtertype}({str(self.filteritem)})"
    def match(self,item):
        if self.filtertype == "person":
            impitem = item.person
        elif self.filtertype == "team":
            impitem = item.team
        elif self.filtertype == "function":
            impitem = item.function
        else:
            return True
        return self.whatever or self.filteritem.id == "*" or self.filteritem.name == "*" or self.filteritem == impitem