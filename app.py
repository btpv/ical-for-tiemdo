import os
import pytz
import requests, datetime,yaml
from yaml.loader import SafeLoader
from icalendar import Calendar, Event, vText
from flask import Flask, Response, abort, request
from multiprocessing import Process,Manager
import classes
last = {}
app = Flask(__name__)
print(yaml.__version__)
with open(f"{os.path.dirname(__file__)}/../hash.yaml") as f:
    hash = yaml.load(f.read(),Loader=SafeLoader)
teams,Functions,people = requests.get(f"https://server-t01.tiemdo.com/phpsepcallsmobile/getTeams.php?DB=nyphoogvliet&user=btpv&hash={hash['rooster']}").json(),requests.get(f"https://server-t01.tiemdo.com/phpsepcallsmobile/getFunctions.php?DB=nyphoogvliet&user=btpv&hash={hash['rooster']}").json(),requests.get(f"https://server-t01.tiemdo.com/phpsepcallsmobile/getAllAccounts.php?DB=nyphoogvliet&id=173&hash={hash['people']}").json()


def getrooster(i,return_dict = {}):
    # print(f"start req {i}")
    return_dict[i] = requests.get(f"https://server-t01.tiemdo.com/phpsepcallsmobile/getTeamPlanner.php?DB=nyphoogvliet&user=btpv&date={(datetime.datetime.now()+ datetime.timedelta(days=i)).strftime('%y-%m-%d')}&hash={hash['rooster']}").json()
    # print(f"  end req {i}")
    return return_dict[i]
def getroosterrange(range,array):
    manager = Manager()
    return_dict = manager.dict()
    pros = []
    for i in range:
        pros.append(Process(target=getrooster, args=(i,return_dict)))
    for p in pros:
        p.start()
    for p in pros:
        p.join()
    for item in list(dict(sorted(return_dict.items())).values()):
        if item != None:
            array.append(item)
    return not all(v is None for v in return_dict.values())
def getroosterend(start,array):
    i = 0
    while getroosterrange(range(start+i,start+i+14),array):
        i+=14
@app.route("/", methods=["GET", "POST"])
def genical():
    try:
        requests.get(f"https://server-t01.tiemdo.com")
    except:
        abort(500)
    name = request.args.get("name","").lower()
    title = request.args.get("title","")
    person = classes.Person(people=people,name=name)
    filter = classes.filter({"person":people},name=name,filtertype="person")
    parent = title == "borro werk"
    if title == "":
        title = "nyp"
    if person.id == "" or person.id == "*" or person.id == "None":
        abort(405)
    weekplan = []
    getroosterrange(range(-14,0),weekplan)
    getroosterend(0,weekplan)
    me = []
    all = []
    for i in weekplan:
        for y in i:
            item = classes.scheduleitem(func=Functions,people=people,teams=teams,raw=y)
            all.append(item)
            if filter.match(item):
                me.append(item)
    cal = Calendar()
    cal.add('prodid', f"{name}tiemdo")
    cal.add('version', f'{datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S")}')
    for i in me:
        event = Event()
        event.add('summary', f"{title}")
        discripton = f'function: {i.function}\nteam: {i.team}'
        if not parent:
            for a in all:
                if a.date == i.date and a.team == i.team:
                    discripton += f"\n{a.person.name} {a.starttime} {a.endtime}"
        discripton += f"\nlast update {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}"
        event.add('description', discripton,encode=False)
        event.add('dtstart', datetime.datetime.strptime(f"{i.date} {i.starttime}", "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.timezone('Europe/Amsterdam')))
        event.add('dtend', datetime.datetime.strptime(f"{i.date} {i.endtime}", "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.timezone('Europe/Amsterdam')))
        event.add('LAST-MODIFIED', datetime.datetime.now())
        event['uid'] = f'{i.id}'
        event.add('priority', 5)
        cal.add_component(event)
    ical = str(cal.to_ical())[2:-1].strip()
    return Response("%s" % ical.replace("\\r\\n", "\n").replace("\\\\","\\").replace("\\xc3\\xab","ë").replace("\\xc3\\x","ë").replace("\\xc3\xa9","é"), mimetype='text/calendar')
if __name__ == "__main__":
    app.run("0.0.0.0", 7979, False)
else:
    def start():
        return app
